package com.ngethe.xmpptest.xmpp.single_chat;

import org.jivesoftware.smack.SmackException;
import org.jivesoftware.smack.XMPPConnection;
import org.jivesoftware.smack.chat2.Chat;
import org.jivesoftware.smack.chat2.ChatManager;
import org.jivesoftware.smack.chat2.IncomingChatMessageListener;
import org.jivesoftware.smack.chat2.OutgoingChatMessageListener;
import org.jxmpp.jid.EntityBareJid;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SingleChatManager {
    private static Map<XMPPConnection, SingleChatManager> connectionSingleChatManagerHashMap = new HashMap<>();

    private WeakReference<XMPPConnection> connectionWeakReference;
    private List<SingleChatListener> listeners = new ArrayList<>();
    private final ChatManager chatManager;

    public static SingleChatManager getInstanceFor(XMPPConnection connection) {
        SingleChatManager singleChatManager = connectionSingleChatManagerHashMap.get(connection);
        if (singleChatManager == null) {
            singleChatManager = new SingleChatManager(connection);
            connectionSingleChatManagerHashMap.put(connection, singleChatManager);
        }
        return singleChatManager;
    }

    private SingleChatManager(XMPPConnection connection) {
        this.connectionWeakReference = new WeakReference<>(connection);
        chatManager = ChatManager.getInstanceFor(connection);
        chatManager.addIncomingListener(xmppIncomingMessageListener);
        chatManager.addOutgoingListener(xmppOutgoingMessageListener);
    }

    public void addSingleChatListener(SingleChatListener listener) {
        listeners.add(listener);
    }

    public void removeSingleChatListener(SingleChatListener listener) {
        listeners.remove(listener);
    }

    public void sendMessage(EntityBareJid buddyJid, String message) throws SmackException.NotConnectedException, InterruptedException {
        Chat chat = chatManager.chatWith(buddyJid);
        chat.send(message);
    }


    private IncomingChatMessageListener xmppIncomingMessageListener = new IncomingChatMessageListener() {
        @Override
        public void newIncomingMessage(EntityBareJid from, org.jivesoftware.smack.packet.Message message, Chat chat) {
            for (int i = 0; i < listeners.size(); i++) {
                SingleChatListener listener = listeners.get(i);
                listener.incomingP2PMessage(from, message, chat);
            }
        }
    };

    private OutgoingChatMessageListener xmppOutgoingMessageListener = new OutgoingChatMessageListener() {
        @Override
        public void newOutgoingMessage(EntityBareJid to, org.jivesoftware.smack.packet.Message message, Chat chat) {
            for (int i = 0; i < listeners.size(); i++) {
                SingleChatListener listener = listeners.get(i);
                listener.outgoingP2PMessage(to, message, chat);
            }
        }
    };
}
