package com.ngethe.xmpptest.xmpp.mucsub;

import androidx.annotation.NonNull;

import org.jivesoftware.smack.packet.IQ;
import org.jxmpp.jid.Jid;

public class MucSubUnsubscribeIQ extends IQ {
    private static final String CHILD_ELEMENT_NAME = "unsubscribe";
    private static final String MUCSUB_NAMESPACE = "urn:xmpp:mucsub:0";

    private String userJid;

    public MucSubUnsubscribeIQ(@NonNull Jid currentUserJid, @NonNull Jid groupJid, String userJid) {
        super(CHILD_ELEMENT_NAME, MUCSUB_NAMESPACE);
        this.userJid = userJid;
        setTo(groupJid);
        setFrom(currentUserJid);
        setType(Type.set);
    }

    public MucSubUnsubscribeIQ(@NonNull Jid currentUserJid, @NonNull Jid groupJid) {
        this(currentUserJid, groupJid, null);
    }

    @Override
    protected IQChildElementXmlStringBuilder getIQChildElementBuilder(IQChildElementXmlStringBuilder xml) {
        if (userJid != null) xml.attribute("jid", userJid);
        return xml;
    }
}
