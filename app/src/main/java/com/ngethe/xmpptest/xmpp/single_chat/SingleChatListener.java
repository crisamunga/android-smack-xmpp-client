package com.ngethe.xmpptest.xmpp.single_chat;

import org.jivesoftware.smack.chat2.Chat;
import org.jxmpp.jid.EntityBareJid;

public interface SingleChatListener {
    void incomingP2PMessage(EntityBareJid from, org.jivesoftware.smack.packet.Message message, Chat chat);
    void outgoingP2PMessage(EntityBareJid to, org.jivesoftware.smack.packet.Message message, Chat chat);
}
